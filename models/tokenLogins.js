'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tokenLogins extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tokenLogins.init({
    loginableId: DataTypes.INTEGER,
    loginableType: DataTypes.STRING,
    tokenId: DataTypes.UUID,
    tokenSecret: DataTypes.TEXT,
    isTokenDeleted: DataTypes.BOOLEAN,
    loggedInAt: DataTypes.DATE,
    isLoggedOut: DataTypes.BOOLEAN,
    loggedOutAt: DataTypes.DATE,
    ipAddress: DataTypes.STRING,
    device: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'token_logins',
  });
  return tokenLogins;
};