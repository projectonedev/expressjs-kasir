const bcrypt = require('bcrypt')

const hashPassword = (password) => {
    const salt = bcrypt.genSaltSync(+process.env.ROUND_SALT);
    return bcrypt.hashSync(password, salt)
}

module.exports = hashPassword