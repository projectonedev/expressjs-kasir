const jwt = require('jsonwebtoken')
const customId = require('custom-id')
const bcrypt = require('bcrypt')
const { Op } = require("sequelize");

const models = require('../models/index')
const tokenBlacklists = models.token_blacklists
const tokenLogins = models.token_logins

const avaliable_auth_type = [
  'user',
  'admin',
  'store_employee'
]

const checkAuthType = (auth_type, res) => {
  // check auth type
  if (!avaliable_auth_type.includes(auth_type)) {
    return res.status(500).json({
      status: false,
      message: '\'Auth Type\' salah. Tolong hubungi Admin!'
    })
  }
}

const auth = (auth_type, req, res, next) =>
  function (req, res, next) {
    // check auth type
    checkAuthType(auth_type, res)

    // check authorize token
    const _token = req.headers['authorization']
    if (!_token) {
        return res.status(400).json({
            status: false,
            message: 'Tidak memiliki izin!'
        })
    }

    try {
        const token = _token.replace('Bearer ', '')

        tokenBlacklists.findOne({ where: { token: token} })
          .then((found) => {
            if (found) {
              return res.status(400).json({
                status: false,
                login: false,
                message: 'Anda telah logout!'
              })
            }

            jwt.verify(token, process.env.JWT_KEY, async (err, payload) => {
              if (err) {
                return res.status(400).json({
                  status: false,
                  login: false,
                  message: 'Tidak memiliki izin!'      
                })
              }

              if (payload) {
                const tokenLogin = await tokenLogins.findOne({
                  where: {
                    [`loginableType`]: payload.loginableType,
                    [`loginableId`]: payload.loginableId,
                    tokenId: payload.token_id
                  }
                })

                if (tokenLogin == null){
                  return res.status(400).json({
                    status: false,
                    login: false,
                    message: 'Tidak memiliki izin!'      
                  })  
                }

                if (tokenLogin.isTokenDeleted) {
                  if (!tokenLogin.isLoggedOut) {
                    tokenLogin.isLoggedOut = true
                    tokenLogin.loggedOutAt = Date.now()
                    
                    await tokenLogin.save()
                  }

                  const _tokenBlacklist = await tokenBlacklists.create({
                    token: token
                  })

                  return res.status(400).json({
                    status: false,
                    login: false,
                    message: 'Anda telah logout!'
                  })
                }
              }

              req.auth = payload
              req.token = token

              next()
            })            
          })
    } catch (err) {
        return res.status(400).json({
            status: false,
            message: err.message
        })
    }
  }

const createToken = async function (req) {
  const token_id = await customId({
    user_id: req.auth.id,
    date: Date.now(),
    randomLength: 4
  })

  var ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress
  
  const delete_token_same_device = false
  if (delete_token_same_device) {
    const _tokenLogins = await tokenLogins.findAll({
      where: {
        loginableType: req.auth.type,
        loginableId: req.auth.id,
        tokenDeleted: false,
        ip_address: ip,
        device: req.headers['user-agent']
      }
    })

    _tokenLogins.forEach(async(tokenLogin) => {
      if (tokenLogin) {
        tokenLogin.isTokenDeleted = true
        await login.save()
      }
    })
  }

  const token_secret = await customId({
    token_secret: ip,
    date : Date.now(),
    randomLength: 8
  })

  const tokenLogin = await tokenLogins.create({
    loginableType: req.auth.type,
    loginableId: req.auth.id,
    tokenId: token_id,
    tokenSecret: token_secret,
    ipAddress: ip,
    device: req.headers['user-agent'],
  })

  const token_user = {
    loginableType: req.auth.type,
    loginableId: req.auth.id,
    token_id: token_id
  }

  const token = await jwt.sign(token_user, process.env.JWT_KEY)
  return token
}

const createAuth = (auth_type) =>
  async function (req, res, next) {
    // check auth type
    checkAuthType(auth_type, res)

    const {username, password} = req.body

    // Validate input
    if (!(username && password)) {
      res.status(500).json({
        status: false,
        message: 'All input is required'
      })
    }

    const user = await models[`${auth_type}s`].findOne({
      where: {
        username: username
      }
    });

    if (user != null) {
      const valid_password = await bcrypt.compare(password, user.password)
      
      if (!valid_password) {
        return res.status(400).json({
          status: false,
          message: 'Password salah!'
        })
      }

      req.auth = {
        type: auth_type,
        id: user.id
      }

      const token = await createToken(req)
      req.token = token
      res.append('X-Authorization', token)

      return next();
    } else {
      return res.status(400).json({
        status: false,
        message: 'Username tidak ditemukan!'
      })
    }
  }

const deleteAuth = (auth_type) =>
  async function (req, res, next) {
    // check auth type
    checkAuthType(auth_type, res)

    const tokenLogin = await models.token_logins.findOne({
      where: {
        [`loginableType`]: req.auth.loginableType,
        [`loginableId`]: req.auth.loginableId,
        tokenId: req.auth.token_id
      }
    })

    if (tokenLogin == null){
      return res.status(400).json({
        status: false,
        message: 'Tidak memiliki izin!'      
      })  
    }

    tokenLogin.isLoggedOut = true
    tokenLogin.loggedOutAt = Date.now()
    tokenLogin.isTokenDeleted = true

    await tokenLogin.save()
    await tokenBlacklists.create({
      token: req.token
    })

    next()
  }

const authUser = async function(req, res, next) {
  const user = await models[`${req.auth.loginableType}s`].findOne({
    where: {
      id: req.auth.loginableId
    },
    attributes: {
      exclude: ['password']
    }
  })
  req.auth.user = user

  next()
}

const logoutAllToken = async function(req, logoutCurrentDevice = false) {
  const loginableType = req.auth.loginableType
  const loginableId = req.auth.loginableId
  const token_id = req.auth.token_id

  const whereData = {
    loginableType: loginableType,
    loginableId: loginableId,
    isTokenDeleted: false,
    token_id: {}
  }

  if (!logoutCurrentDevice) {
    whereData.tokenId = {
      [Op.ne]: token_id
    }
  }

  const _tokenLogins = await tokenLogins.findAll({
    where: {
      loginableType: loginableType,
      loginableId: loginableId,
      isTokenDeleted: false,
      tokenId: (!logoutCurrentDevice) ? {
        [Op.ne]: token_id
      } : { [Op.ne]: null }
    }
  })

  _tokenLogins.forEach(async(tokenLogin) => {
    if (tokenLogin) {
      tokenLogin.isTokenDeleted = true
      await tokenLogin.save()
    }
  })
}

module.exports = { auth, createAuth , deleteAuth, authUser, logoutAllToken}