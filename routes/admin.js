const express = require('express')

// Controller
const authController = require('../controllers/admin/authController')
const profileController = require('../controllers/admin/profileController')
const loginsController = require('../controllers/admin/loginsController')
const adminsController = require('../controllers/admin/adminsController')

// Middleware
const {auth, createAuth, deleteAuth, authUser} = require('../middlewares/auth')

// Model
const model = require('../models/index')
const admins = model.admins

const router = express.Router()

// Auth Router
// login
router.post('/auth/login', createAuth('admin'), authController.login)

// logout
router.post('/auth/logout', auth('admin'), deleteAuth('admin'), authController.logout)

// change password
router.post('/auth/change_password', auth('admin'), authController.changePassword)

// Auth Logins Router
// logins active
router.get('/auth/logins/active', auth('admin'), loginsController.active)

// logins deleted
router.get('/auth/logins/deleted', auth('admin'), loginsController.deleted)

// logins delete
router.post('/auth/logins/delete', auth('admin'), loginsController.delete)

// logins delete-all
router.post('/auth/logins/delete_all', auth('admin'), loginsController.deleteAll)

// Profile Router
// show
router.get('/profile', auth('admin'), authUser, profileController.show)

// update
router.post('/profile/update', auth('admin'), profileController.update)

// Admins Router
router.post('/admins/detail', auth('admin'), adminsController.detail)

// Admins Router
router.get('/admins/show', auth('admin'), adminsController.show)

// Admins Router
router.post('/admins/create', auth('admin'), adminsController.create)

// Admins Router
router.post('/admins/update', auth('admin'), adminsController.update)

// Admins Router
router.post('/admins/delete', auth('admin'), adminsController.delete)

// Admins Router
router.post('/admins/check_username', auth('admin'), adminsController.checkUsername)

// Admins Router
router.post('/admins/check_email', auth('admin'), adminsController.checkEmail)

module.exports = router