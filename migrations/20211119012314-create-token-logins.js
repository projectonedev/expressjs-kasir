'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('token_logins', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      loginableId: {
        type: Sequelize.INTEGER
      },
      loginableType: {
        type: Sequelize.STRING
      },
      tokenId: {
        type: Sequelize.UUID
      },
      tokenSecret: {
        type: Sequelize.TEXT
      },
      isTokenDeleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      loggedInAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now')
      },
      isLoggedOut: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      loggedOutAt: {
        type: Sequelize.DATE
      },
      ipAddress: {
        type: Sequelize.STRING
      },
      device: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('token_logins');
  }
};