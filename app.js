require("dotenv").config();

const express = require('express')
const cors = require('cors')

const app = express()
const port = process.env.APP_PORT

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Importing Routes
const adminRouter = require('./routes/admin')
app.use(cors())
// Using Importing Routes
app.use('/admin', adminRouter)

app.get('/', (req, res) => {
  res.json({
    status: true,
    name: 'Api Kasir',
    description: 'Api Kasir by ProjectOneDev',
    contact: {
      instagram: '@projectone.dev',
      whatsapp: '+62-877-2886-4687'
    }
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.use(function (req, res) {
  return res.status(404).json({
    status: false,
    message: 'Nyari apa cuy!'
  })
})