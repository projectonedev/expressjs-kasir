'use strict';
require("dotenv").config();
const hashPassword = require('../utils/password')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const fields = [{
      username: 'admin',
      password: hashPassword('project01'),
      name: 'Admin',
      email: 'pod.projectonedev@gmail.com',
      status: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }]

    return queryInterface.bulkInsert('admins', fields, {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('admins', [{
      username: 'admin'
    }], {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
