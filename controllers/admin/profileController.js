const { Op } = require('sequelize')
const Joi = require('joi')

// Model
const models = require('../../models/index')
const admins = models.admins

exports.show = function (req, res) {
  return res.json({
    status: true,
    admin: req.auth.user,
  })
}

exports.update = async function (req, res) {
  const schema = Joi.object({
    name: Joi.string()
      .pattern(/^[a-zA-Z\-\s]+$/, 'alpha-spaces'),
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30),
    email: Joi.string()
      .email()
      .lowercase()
  }).or('username', 'email')

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { username, email, name } = value

  const loginableId = req.auth.loginableId

  const admin = await admins.findOne({
    where: { id: loginableId },
    attributes: {
      exclude: ['password']
    }
  })

  if (username) {
    const usernameCount = await admins.count({
      where: {
        username: username,
        id: {
          [Op.ne]: loginableId
        }
      },
      paranoid: false
    })

    if (usernameCount > 0) {
      return res.status(400).json({
        status: false,
        message: 'Username tidak tersedia!',
      })
    }
  }

  if (email) {
    const emailCount = await admins.count({
      where: {
        email: email,
        id: {
          [Op.ne]: loginableId
        }
      },
      paranoid: false
    })

    console.log(emailCount)

    if (emailCount > 0) {
      return res.status(400).json({
        status: false,
        message: 'Email tidak tersedia!',
      })
    }
  }

  admin.name = name
  admin.username = username
  admin.email = email
  admin.save()

  return res.json({
    status: true,
    message: 'Profil berhasil diubah.',
    admin: admin,
  })

}