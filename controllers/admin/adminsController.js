const { Op } = require('sequelize')
const Joi = require('joi')

// Model
const models = require('../../models/index')
const admins = models.admins

const hashPassword = require('../../utils/password')

exports.show = async function (req, res) {
  const limit = 5;
  const page = (req.query.page && !isNaN(req.query.page)) ? req.query.page : 1
  const search = req.query.search

  const where = (search) ? {
    [Op.or]: [
      { name: {
        [Op.like]: `%${search}%`
      }},
      { username: {
        [Op.like]: `%${search}%`
      }},
      { email: {
        [Op.like]: `%${search}%`
      }},
    ]
  } : {}
  const total_data = await admins.count({
    where: where,
  })

  const max_pages = Math.ceil(total_data / limit)

  if (page > max_pages) {
    return res.json({
      status: true,
      admins: [],
      total_data: 0,
      page: 1,
      max_pages: 1  
    })
  }

  const _admins = await admins.findAll({
    where: where,
    
    attributes: {
      exclude: ['password']
    },
    offset: limit * (page - 1),
    limit: limit
  })

  return res.json({
    status: true,
    admins: _admins,
    total_data: total_data,
    page: page,
    max_pages: max_pages
  })
}

exports.detail = async function (req, res) {
  const id = req.body.id

  console.log(id);

  if (isNaN(id)) {
    return res.status(400).json({
      status: false,
      message: 'Format id salah'
    })  
  }

  const admin = await admins.findAll({
    where: {id: id},
    attributes: {
      exclude: ['password']
    },
  })

  if (admin.length == 0) {
    return res.status(400).json({
      status: false,
      message: 'Id tidak ditemukan.'
    })  
  }

  return res.json({
    status: true,
    admin: admin[0],
  })
}

exports.create = async function (req, res) {
  const schema = Joi.object({
    name: Joi.string()
      .pattern(/^[a-zA-Z\-\s]+$/, 'alpha-spaces'),
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    password: Joi.string()
      .min(6)
      .required(),
    repeat_password: Joi.ref('password'),
    email: Joi.string()
      .email()
      .lowercase()
      .required()
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { username, password, name, email } = value

  const usernameCount = await admins.count({
    where: {
      username: username,
    },
    paranoid: false
  })

  if (usernameCount > 0) {
    return res.status(400).json({
      status: false,
      message: 'Username sudah dipakai.',
    })
  }

  const emailCount = await admins.count({
    where: {
      email: email,
    },
    paranoid: false
  })

  if (emailCount > 0) {
    return res.status(400).json({
      status: false,
      message: 'Email sudah dipakai.',
    })
  }


  await admins.create({
    name: name,
    username: username,
    password: hashPassword(password),
    email: email
  }).then(item => {
    return res.json({
      status: true,
      message: 'Admin berhasil dibuat.',
      data: item
    })
  }).catch(error => {
    return res.status(400).json({
      status: false,
      message: error.message
    })
  })
}

exports.update = async function (req, res) {
  const schema = Joi.object({
    id: Joi.number()
      .required(),
    name: Joi.string()
      .pattern(/^[a-zA-Z\-\s]+$/, 'alpha-spaces'),
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30),
    email: Joi.string()
      .email()
      .lowercase(),
    status: Joi.boolean()
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { id, name, username, email, status } = value

  const admin = await admins.findOne({
    where: { id: id },
    attributes: {
      exclude: ['password']
    }
  })

  if (admin == null) {
    return res.status(400).json({
      status: false,
      message: '\'id\' tidak ditemukan!',
    })
  }

  if (username != null) {
    const usernameCount = await admins.count({
      where: {
        username: username,
        id: {
          [Op.ne]: id
        }
      },
      paranoid: false
    })

    if (usernameCount > 0) {
      return res.status(400).json({
        status: false,
        message: 'Username sudah dipakai.',
      })
    }
  }

  if (email != null) {
    const emailCount = await admins.count({
      where: {
        email: email,
        id: {
          [Op.ne]: id
        }
      },
      paranoid: false
    })

    if (emailCount > 0) {
      return res.status(400).json({
        status: false,
        message: 'Email sudah dipakai.',
      })
    }
  }

  admin.name = name
  admin.username = username
  admin.email = email
  admin.status = status
  admin.save()

  return res.json({
    status: true,
    message: 'Data Admin berhasil diubah.',
    admin: admin
  })
}

exports.delete = async function (req, res) {
  const schema = Joi.object({
    id: Joi.number()
      .required()
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { id } = value

  const admin = await admins.findByPk(id)

  if (admin == null) {
    return res.status(400).json({
      status: false,
      message: '\'Id\' tidak ditemukan!',
    })
  }

  admin.destroy()

  return res.json({
    status: true,
    message: 'Admin berhasil dihapus.',
  })
}

exports.checkUsername = async function (req, res) {
  const schema = Joi.object({
    username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    currentUsername: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { username, currentUsername } = value

  if (username == currentUsername) {
    return res.json({
      status: true,
      message: 'Username dapat digunakan.',
    })
  }

  const usernameCount = await admins.count({
    where: {
      username: username,
    },
    paranoid: false
  })

  if (usernameCount > 0) {
    return res.status(400).json({
      status: false,
      message: 'Username sudah dipakai.',
    })
  }

  return res.json({
    status: true,
    message: 'Username dapat digunakan.',
  })

}

exports.checkEmail = async function (req, res) {
  const schema = Joi.object({
    email: Joi.string()
      .email()
      .lowercase()
      .required()
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { email } = value

  const emailCount = await admins.count({
    where: {
      email: email,
    },
    paranoid: false
  })

  if (emailCount > 0) {
    return res.status(400).json({
      status: false,
      message: 'Email sudah dipakai.',
    })
  }

  return res.json({
    status: true,
    message: 'Email dapat digunakan.',
  })

}