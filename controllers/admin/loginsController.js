const { Op } = require('sequelize')
const Joi = require('joi')

// Model
const models = require('../../models/index')
const admins = models.admins
const tokenLogins = models.token_logins
const tokenBlacklists = models.token_blacklists

const { logoutAllToken } = require('../../middlewares/auth')

exports.active = async function (req, res) {
  const _tokenLogins = await tokenLogins.findAll({
    attributes: [
      'id', 'device', 'ipAddress', 'loggedInAt'
    ],
    where: {
      loginableType: req.auth.loginableType,
      loginableId: req.auth.loginableId,
      isTokenDeleted: false
    }
  })

  return res.json({
    status: true,
    logins: _tokenLogins
  })
}

exports.deleted = async function (req, res) {
  const id = req.auth.loginableId

  const _tokenLogins = await tokenLogins.findAll({
    where: {
      loginableId: id,
      isTokenDeleted: true
    }
  })

  return res.json({
    status: true,
    logins: _tokenLogins
  })
}

exports.delete = async function (req, res) {
  const schema = Joi.object({
    id: Joi.number()
      .required()
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { id } = value

  const tokenLogin = await tokenLogins.findOne({
    where: {
      id: id,
      loginableType: req.auth.loginableType,
      loginableId: req.auth.loginableId,
    }
  })

  if (tokenLogin == null) {
    return res.status(400).json({
      status: false,
      message: '\'Id\' tidak ditemukan!'
    })
  }


  if (tokenLogin.isTokenDeleted == true) {
    return res.status(400).json({
      status: false,
      message: '\'Data Login\' sudah terhapus sebelumnya!'
    })
  }

  tokenLogin.update({
    isTokenDeleted: true
  })

  tokenLogin.save()

  return res.json({
    status: true,
    message: '\'Data Login\' berhasil dihapus.'
  })
}

exports.deleteAll = function (req, res) {
  const schema = Joi.object({
    with_current: Joi.boolean()
      .default(false)
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { with_current } = value

  logoutAllToken(req, with_current)

  return res.json({
    status: true,
    message: 'Semua \'Data Login\' berhasil dihapus'
  })
}