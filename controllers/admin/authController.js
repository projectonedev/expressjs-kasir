const bcrypt = require('bcrypt')
const Joi = require('joi')

// Password Utils
const hashPassword = require('../../utils/password')

// Model
const models = require('../../models/index')
const admins = models.admins

const { logoutAllToken } = require('../../middlewares/auth')

exports.login = function (req, res) {
  res.json({
    status: true,
    token: req.token,
    message: "Login Berhasil"
  })
}

exports.logout = function (req, res) {
  return res.json({
    status: true,
    message: 'Logged out successfully.'
  })
}

exports.changePassword = async function (req, res) {
  const schema = Joi.object({
    password: Joi.string()
      .min(6)
      .required(),
    new_password: Joi.string()
      .min(6)
      .required(),
    repeat_new_password: Joi.ref('new_password'),
  })

  const { error, value } = schema.validate(req.body, {
    abortEarly: false
  })

  if (error) {
    return res.status(400).json({
      status: false,
      input_errors: error
    })
  }

  const { password, new_password } = value

  const admin = await admins.findOne({
    where: {
      id: req.auth.loginableId
    }
  })

  const password_hash = admin.password

  const valid_password = await bcrypt.compare(password, password_hash)

  if (!valid_password) {
    return res.status(400).json({
      status: false,
      message: 'Password Salah'
    })
  }

  if (password == new_password) {
    return res.status(400).json({
      status: false,
      message: 'Password Baru harus berbeda dari password sebelumnya!'
    })
  }

  admin.password = hashPassword(new_password)
  admin.save()

  logoutAllToken(req)

  return res.json({
    status: true,
    message: 'Password berhasil diubah.'
  })
}